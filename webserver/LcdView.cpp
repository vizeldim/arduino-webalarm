#include "LcdView.h"


const char *dowShrtStr[] = { "mon", "tue", "wed", "thu", "fri", "sat", "sun" };


void LcdView::init             ()
{
  this->clear();
}


void LcdView::show             (const char * txt)
{
  this->_lcd.setCursor(0,0);
  this->_lcd.print(txt);  
}

void LcdView::showTime         (TimeView timeView)
{
  // -------- SECONDS -------- 
  int lastSecond = this->lastTime.second;
  if(lastSecond != timeView.second)
  {
    this->lastTime.second = timeView.second;
    this->_lcd.setCursor(6,1);
    this->_lcd.printf("%02d", timeView.second);   
  }


  // -------- MINUNTES -------- 
  int lastMinute = this->lastTime.minute;
  
  if(lastMinute != timeView.minute)
  {
    this->lastTime.minute = timeView.minute;
    this->_lcd.setCursor(3,1);
    this->_lcd.printf("%02d", timeView.minute);
  }

  
  // -------- HOURS --------
  int lastHour = this->lastTime.hour;
  if(lastHour != timeView.hour)
  {
    this->lastTime.hour = timeView.hour;
    this->_lcd.setCursor(0,1);
    this->_lcd.printf("%02d", timeView.hour);
  }

  
  // DELIMITERS = DOUBLE DOTS
  if(!(this->timeDelimsVisible))
  {
    this->timeDelimsVisible = true;
    
    this->_lcd.setCursor(2,1);
    this->_lcd.print(":");
  
    this->_lcd.setCursor(5,1);
    this->_lcd.print(":");  
  }
}


void LcdView::showDate         (DateView dateView)
{ 
  // -------- DAYS -------- 
  int lastDay = this->lastDate.day;
  if(lastDay != dateView.day)
  {
    this->lastDate.day = dateView.day;
    this->_lcd.setCursor(0,0);
    this->_lcd.printf("%02d", dateView.day); 
  }  

  // -------- DOW -------- 
  if(this->lastDate.dow != dateView.dow)
  {
    this->lastDate.dow = dateView.dow;
    this->_lcd.setCursor(11,0);
  
    if(dateView.dow < 0 || dateView.dow >= 7) 
    {
      this->_lcd.printf("ERR - DateTimeView.dow = %d => invalid", dateView.dow);
    }
    else
    {
      this->_lcd.print(dowShrtStr[dateView.dow]);
    }
  }

  // -------- MONTH -------- 
  int lastMonth = this->lastDate.month;
  if(lastMonth != dateView.month)
  {
    this->lastDate.month = dateView.month;
    this->_lcd.setCursor(3,0);
    this->_lcd.printf("%02d", dateView.month); 
  }


  // -------- YEAR -------- 
  int lastYear = this->lastDate.year;
  if(lastYear != dateView.year)
  {
    this->lastDate.year = dateView.year;
    this->_lcd.setCursor(6,0);
    this->_lcd.printf("%04d", dateView.year); 
  }

  // DELIMITERS = DOTS
  if(!(this->dateDelimsVisible))
  {
    this->dateDelimsVisible = true;
    
    this->_lcd.setCursor(2,0);
    this->_lcd.print(".");
  
    this->_lcd.setCursor(5,0);
    this->_lcd.print("."); 
  }
}

void LcdView::showDateTime     (TimeView timeView, 
                                DateView dateView)
{
  this->showTime(timeView);
  this->showDate(dateView);
}
                              
void LcdView::showTemperature  (TemperatureView temperatureView)
{
  float lastTemperature = this->lastTemperature.temperature;
  float curTemperature = temperatureView.temperature;

  if(curTemperature < 0 || curTemperature > 100)
  {
    this->_lcd.printf("ERR - temperatureView.temperature = %d => invalid range", curTemperature);
    return;
  }
  
  int lastTemperatureCmp = (int)(lastTemperature * 100 + .5);
  int curTemperatureCmp = (int)(curTemperature * 100 + .5);

  
  if(lastTemperatureCmp != curTemperatureCmp)
  {
    this->lastTemperature.temperature = curTemperature;
    int tempPos = 9;
    this->_lcd.setCursor(tempPos, 1);
    this->_lcd.printf("%.2f", curTemperature);
  
    int unitPos = tempPos + 1 + this->numLen(curTemperatureCmp);
    this->_lcd.setCursor(unitPos, 1);
    this->_lcd.print(" C");
  }
}

int LcdView::numLen(int num)
{
  int len=!num;
  while(num){ ++len; num/=10; }
  return len;
}

void LcdView::clear        ()
{
  this->_lcd.clear();  
  this->timeDelimsVisible = false;
  this->dateDelimsVisible = false;

  this->lastTime = TimeView{-1, -1, -1};
  this->lastDate = DateView{-1, -1, -1, -1};
  this->lastTemperature = TemperatureView{0};
}
