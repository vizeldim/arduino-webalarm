#pragma once

#include <DS3231.h>
#include "DateTimeInterface.h"

class RtcDateTime : public DateTimeInterface
{
  public:
    RtcDateTime(DS3231 & rtc) : _rtc(rtc){}
    mTime getTime();  
    mDate getDate();
    int getDow();
    bool setDateDowTime(mDate newDate, int dow, mTime newTime);
  private:
    DS3231& _rtc; 
};
