#pragma once

struct mAlarm
{
  int   hour = -1;
  int   minute;
  bool  dows[7];
  bool  isEnabled = false;
};

struct mAlarmList
{
  mAlarm arr[8] {};
};

class StorageInterface
{
  public:
  virtual void         clearMem         ()                      = 0;
  virtual void         init             ()                      {}
             
  virtual const mAlarmList&   getAlarms ()                      = 0;
  
  virtual int          saveAlarm        (mAlarm alarm)          = 0;  

  virtual bool         validateAlarm    (const mAlarm &alarm)   = 0;
  
  virtual bool         existsById       (int id)                = 0;
  
  virtual bool         removeAlarm      (int id)                = 0;
  
  virtual bool         disableAlarm     (int id)                = 0;
  
  virtual bool         enableAlarm      (int id)                = 0;

  virtual bool         changeAlarmDows  (int id, 
                                         bool dows[7])          = 0;

  virtual bool         editAlarm        (int id, 
                                         mAlarm newAlarm)       = 0;

  virtual bool         changeAlarmHour  (int id, int hour)      = 0;

  virtual bool         changeAlarmMinute(int id, int minute)    = 0;
 
  virtual mAlarm       getNextAlarm     (int hour, 
                                         int minute, 
                                         int dow)               = 0;

  virtual void         updateNext       ()                      = 0;
 
};
