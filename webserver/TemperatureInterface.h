#pragma once

class TemperatureInterface
{
  public:
    virtual void  init()           {}
    virtual float getTemperature() = 0;
};
