
class TestDataClient{
    constructor(connected) {
        this._conn = connected
        this._ip = localStorage.getItem('WebClockStorageTag')
    }
    changeToDomain() {

    }
    changeToIp () {

    }

    updateIp(newIp) {
        this._ip = newIp
        this.changeToIp()
        localStorage.setItem('WebClockStorageTag', newIp)
    }
    testConn() {
        return new Promise((res, rej) => {
            setTimeout(()=>{
                if(this._conn)
                {
                    res()
                }
                else
                {
                    rej("e")
                }
            }, 2000)
        })
    }

    getAlarms() {
        console.log("API CALL ===================== GET ALL ALARMS")
        return new Promise((res, rej) => {
            setTimeout(()=>{
                if(this._conn)
                {
                    res(JSON.parse(JSON.stringify(testAlarms)))
                }
                else
                {
                    rej("e")
                }
            }, 2000)
        })
    }

    getDDT() {
        console.log("API CALL ===================== GET DDT")
        return new Promise((res, rej) => {
            const d = getDateFromObj(testDDT)

            setTimeout(()=>{
                if(this._conn)
                {
                    res(d)
                }
                else
                {
                    rej("e")
                }
                
            }, 2000)
        })
    }

    putDDT(ddt) {
        console.log("API CALL ===================== SET DDT", ddt)
        console.log("BEFORE", testDDT)

        testDDT.year = ddt.year
        testDDT.month = ddt.month
        testDDT.day = ddt.day
        testDDT.hour = ddt.hour
        testDDT.minute = ddt.minute
        testDDT.second = ddt.second

        return new Promise((res, rej) => {

            setTimeout(()=>{
                if(this._conn)
                {
                    res("OK")
                }
                else
                {
                    rej("e")
                }
            }, 2000)
            

            console.log("AFTER", testDDT)
        })
    }

    postAlarm(alarm) {
        console.log("API CALL ===================== POST ALARM", alarm)

        console.log("BEFORE", testAlarms)

        return new Promise((res, rej) => {
            const newId = testAlarms.alarms.length + 1
            alarm.id = newId
            testAlarms.alarms.push(alarm)

            setTimeout(()=>{
                if(this._conn)
                {
                    res(newId)
                }
                else
                {
                    rej("e")
                }
            }, 2000)
            

            console.log("AFTER", testAlarms)

        })
    }

    patchAlarm(alarm) {
        console.log("API CALL ===================== PATCH ALARM", alarm)
        console.log("BEFORE", JSON.stringify(testAlarms))

        return new Promise((res, rej) => {
            if(!(alarm.id) || isNaN(alarm.id)){
                rej("INVALID ID")
            }
            const found = testAlarms.alarms.find(x => x.id === alarm.id)
            if(!found){
                rej(`Alarm with id = ${alarm.id} not found`)
            } 
            else{
                if("dows" in alarm){
                    found.dows = [...alarm.dows]
                }

                if("hour" in alarm) {
                    found.hour = alarm.hour
                }

                if("minute" in alarm) {
                    found.minute = alarm.minute
                }

                if("isEnabled" in alarm){
                    found.isEnabled = alarm.isEnabled
                }

                setTimeout(()=>{
                    if(this._conn)
                    {
                        res(testAlarms.alarms.length)
                    }
                    else
                    {
                        rej("e")
                    }
                }, 2000)
                
    

                console.log("AFTER", JSON.stringify(testAlarms))

            }
        })
    }
}