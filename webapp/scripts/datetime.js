const dowMap = [
    'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', "Sunday"
]

const dowShrtMap = [
    'mon', 'tue', 'wed', 'thu', 'fri', 'sat', "sun"
]


function toDow(dowN) {
    if(dowN < 0 || dowN >= 7){
        return "UNKNOWN"
    }
    return dowMap[dowN]
}

function toShrtDow(dowN) {
    if(dowN < 0 || dowN >= 7){
        return "UNKNOWN"
    }
    return dowShrtMap[dowN]
}

function twoDigits(num) {
    return (num >= 10) ? num : '0' + num
}

function timeString(t) {
    return `${twoDigits(t.getHours())}:${twoDigits(t.getMinutes())}:${twoDigits(t.getSeconds())}`
}

function dateString(d) {
    return `${twoDigits(d.getDate())}.${twoDigits(d.getMonth() + 1)}.${d.getFullYear()}`
}

function dowString(d) {
    return `${toDow(d.getDay() - 1)}`
}

function getDateFromObj(o) {
    const date = new Date()
    date.setFullYear(o.year, o.month - 1, o.day)
    date.setHours(o.hour)
    date.setMinutes(o.minute)
    date.setSeconds(o.second)

    return date
}

function dateToObj(date) {
    return {
        day: date.getDate(),
        month: date.getMonth() + 1,
        year: date.getFullYear(),
        hour: date.getHours(),
        minute: date.getMinutes(),
        second: date.getSeconds(),
        dow: date.getDay() - 1
    }
}


function validateDDT({day, month, year, hour, minute, second}) {
    if(isNaN(day) || isNaN(month) || isNaN(year) || isNaN(hour) || isNaN(minute) || isNaN(second)){
        return false
    }    

    if(!validateHourMinute({hour, minute})){
        return false
    }

    if(second < 0 || second >= 60){
        return false
    }

    if(year < 0){
        return false
    }

    if(month <= 0 || month >= 13){
        return false
    }

    if(day < 0 || day > 31){
        return false
    }

    return true
}


function validateHourMinute({hour, minute}) {
    if(isNaN(hour) || isNaN(minute)){
        return false
    }    


    if(hour < 0 || hour >= 24) {
        return false
    }

    if(minute < 0 || minute >= 60){
        return false
    }

    return true
}

class DateTime {
    constructor(timeE, dateE, dowE) {
        this.timeE = timeE
        this.dateE = dateE
        this.dowE = dowE

        this.vals = dateToObj(new Date())
        this._offset = 0
        this._started = false
    }

    changeDDT(date) { //param = Date() instance
        this._offset = new Date() - date 
    }

    start() {
        if(!this._started){
            this._started = true
            setInterval(()=>{
                const now = new Date(new Date() - this._offset)
                this.vals = dateToObj(now)
                this.timeE.innerHTML = timeString(now)
                this.dateE.innerHTML = dateString(now)
                this.dowE.innerHTML = dowString(now)
            }, 1000)     
        }  
    }
}