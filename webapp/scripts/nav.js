class Nav{
    constructor(){
        this._pages = [] // {menuItem, page}
    }

    addPage({menuE, pageE}) {
        this._pages.push({menuE, pageE})
        menuE.addEventListener('click', (e) => {
            this.clear()
            menuE.classList.add('nav-item-active')
            pageE.classList.remove('hidden')
        })
        return this
    }

    clear() { 
        this._pages.forEach(p => {
            p.pageE.classList.add('hidden')
            p.menuE.classList.remove('nav-item-active')
        })
    }

}