class App {
    constructor({testMode, success}, apiSource, alarmsHolder, adder, adderContext, ipChangeElem, ddt){
        // === DATA ===
        this._api = testMode ? (new TestDataClient(success)) :
                               (new ApiClient(apiSource))
        this._alarms = []
        this._ddt = ddt
 
        // === UI ===
        this._adder = adder
        this._adderCtx = adderContext
        this._UI_a = new AlarmListUI(alarmsHolder, adder, adderContext, this.switchCb.bind(this))
        this._loader = new Loader()
        this._error = new ErrorMsg()
        this._ipChangeCtx = new IpChangeContext(ipChangeElem)
        this._ipChangeCtx.addOnSaveHandler(
            (e) => {
                e.preventDefault()
                const newIp = this._ipChangeCtx.getSelected()
                console.log(newIp)
                this._api.updateIp(newIp)
                this._ipChangeCtx.close()
                this.connect().then(this.loadExternal.bind(this))
                .catch(()=>{this._apiLoadError(false)})
            }
        )

        this._error.addReconnHandler(() => {
            this._error.close()
            this.connect(true).then(this.loadExternal.bind(this))
            .catch(()=>{this._apiLoadError(false)})
        })

        this._error.addChangeIpHandler(() => {
            this._error.close()
            this._ipChangeCtx.customize(this._api._ip ?? "Unknown")
            this._ipChangeCtx.open(false)
        })

        this.connect(true)
            .then(this.loadExternal.bind(this))
            .catch(()=>{this._apiLoadError(false)})
    }
    
    connect(domain) {
        return new Promise((res, rej) => {
            this._loader.show("Connecting...")
            if(domain){
                this._api.changeToDomain()
            } else{
                this._api.changeToIp()
            }
            
            this._api.testConn(3)
                .then(() => {
                    this._loader.hide()
                    res()
                })
                .catch((e) => {
                    if(this._api.changeToIp()){
                        this._api.testConn(1)
                        .then(() => {
                            this._loader.hide()
                            res()
                        })
                        .catch(() => {
                            rej()
                        })
                    }
                    else {
                        rej()
                    }
                })
        })
    }


    loadExternal() {
        this._loader.show()

        this._api.getDDT().then(data => {
            this._ddt.changeDDT(data)
        })

        this._api.getAlarms().then(data => {
            this.parseAlarmResp(data)
            this.loadUI()

            this._loader.hide()
            if(this._alarms.length < 8) {
                this._showAdder()
            }
            
        }).catch(e => {
            if(this._alarms.length < 8) {
                this._showAdder()
            }

            this._apiLoadError() 
        })
    }

    loadUI() {
        this._UI_a.load(this._alarms)
    }

    parseAlarmResp(resp) {
        this._alarms = resp['alarms']
    }

    _hideAdder() {
        if(this._adder != null) {
            this._adder.classList.add('hidden')
        }
    }

    _showAdder() {
        if(this._adder != null) {
            this._adder.classList.remove('hidden')
        }
    }


    _apiLoadError(closable) {
        this._loader.hide()
        this._error.open(closable)
    }

    registerAdder(adder) {
        this._adder = adder
        if(this._alarms.length >= 8) {
            this._hideAdder()
        }
    }

    findByTime({hour, minute}) {
        return this._alarms.find(x => x.hour == hour && x.minute == minute)
    }

    addAlarm(newAlarm) {
        const found = this.findByTime(newAlarm)

        if(found) {
            console.log("Alarm with this time already exists")
            this._adderCtx.showError("Alarm with this time already exists.")
            return false
        } 

        this._loader.show()
        this._api.postAlarm({
                hour: newAlarm.hour,
                minute: newAlarm.minute,
                dows: newAlarm.dows,
                isEnabled: true
            })
            .then((id) => {
                const added = {
                    id: id,
                    hour: newAlarm.hour,
                    minute: newAlarm.minute,
                    dows: newAlarm.dows,
                    isEnabled: true
                }
                this._UI_a.add(added)
                this._alarms.push(added)
                
                this._loader.hide()
                if(this._alarms.length >= 8) {
                    this._hideAdder()
                }
                
            }).catch(e => { this._apiLoadError() })

        return true
    }


    editAlarm(changedAlarm) {
        const found = this._alarms.find(x => x.id === changedAlarm.id)    

        if(!found) {
            console.log(`ALARM WITH ID = ${changedAlarm.id} NOT FOUND`)
            return 
        }

        const patch = {id: found.id}
        let needPatch = false
        if("hour" in changedAlarm  && changedAlarm.hour !== found.hour) {
            patch.hour = changedAlarm.hour
            needPatch = true
        }
        if("minute" in changedAlarm  && changedAlarm.minute !== found.minute) {
            patch.minute = changedAlarm.minute
            needPatch = true
        }
        if("dows" in changedAlarm && JSON.stringify(changedAlarm.dows.sort()) !== JSON.stringify(found.dows.sort())) {
            patch.dows = [...changedAlarm.dows]
            needPatch = true
        }
        if("isEnabled" in changedAlarm) {
            patch.isEnabled = changedAlarm.isEnabled
            needPatch = true
        }

        if(!needPatch){
            console.log(`NOTHING TO PATCH ON ALARM WITH ID = ${found.id}`)
            return 
        }


        this._loader.show()
        this._api.patchAlarm(patch)
            .then(() => {
                if(changedAlarm.hour) {
                    found.hour = changedAlarm.hour
                }
                if(changedAlarm.hour) {
                    found.minute = changedAlarm.minute
                }
                if(changedAlarm.dows) {
                    found.dows = [...changedAlarm.dows]
                }
                this._UI_a.editById(changedAlarm)

                this._loader.hide()
            }).catch(e => { this._apiLoadError() })
    }

    switchCb(alarmUI, id, isEnabled) {
        this.toggleAlarm(id, isEnabled)
    }


    toggleAlarm(id, isEnabled) {
        const found = this._alarms.find(x => x.id === id) 
        
        this._loader.show()
        this._api.patchAlarm({id, isEnabled: !isEnabled})
            .then(() => {
                found.isEnabled = !isEnabled
                this._UI_a.toggleById(id, !isEnabled)
                this._loader.hide()
            }).catch(e => { this._apiLoadError() })
    }


    editDateTime(ddt) { //{day, month, year, hour, minute, second}
        const date = getDateFromObj(ddt)
        ddt["dow"] = date.getDay() - 1 // ADD + {dow}

        this._loader.show()
        this._api.putDDT(ddt)
            .then((d) => {
                this._ddt.changeDDT(date)
                this._loader.hide()
            }).catch(e => { this._apiLoadError() })
    }
}