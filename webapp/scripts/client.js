class ApiClient{
    constructor({baseUrl, tag}) {
        this._url = baseUrl
        
        this._domain = baseUrl
        this._storageTag = tag
        this._ip = localStorage.getItem(tag)
        console.log(this._storageTag, this._ip)
        this._failRate = 5
        this._fails = 0
    }                        

    updateIp(newIp) {
        console.log("UPDATING .... ", this._domain, this._ip, this._url, this._storageTag)
        this._ip = newIp
        this.changeToIp()
        localStorage.setItem(this._storageTag, newIp)
        console.log("UPDATING .... END ", this._domain, this._ip, this._url, this._storageTag)

    }

    changeToIp() {
        if(this._ip === null){
            return false
        }
        this._url = `http://${this._ip}`
        return true
    }

    changeToDomain() {
        this._url = this._domain
    }

    testConn(failRate) {
        console.log(`TRY TO CONNECT ${this._fails} ${this._url}`)
        return new Promise((res, rej) => {
            fetch(`${this._url}/ip`, {method: 'GET'})
            .then((resp) => {
                res()
                return resp.json
            })
            .then(data => {
                const ip = data['ip']
                if(ip && ip != false && ip != this._ip) {
                    this.updateIp(ip)   
                }
                this._fails = 0
            })
            .catch(err => {
                console.log("CONN ERR :(", err)
                if((failRate ?? this._failRate) > ++(this._fails)){
                    this.testConn(failRate)
                        .catch(() => rej())
                }
                else{
                    this._fails = 0
                    rej(err)
                }
            })
        })
    }


    getDDT() {
        console.log("GET - /datetime")
        
        return new Promise((res, rej) => {
            fetch(`${this._url}/datetime`, {method: 'GET'})
            .then(resp => {
                console.log(resp)
                return resp.json()})
            .then(data => {
                console.log(data)
                const d = getDateFromObj(data)
                res(d)
            })
            .catch(e => {
                console.log("ERROR LOADING"); 
                rej(e)})
        })
    }

    putDDT(ddt){
        console.log("PUT - /datetime", JSON.stringify(ddt))
        return new Promise((res, rej) => {
            fetch(`${this._url}/datetime`, {
                method: 'PUT',
                body: JSON.stringify(ddt),
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    'Access-Control-Allow-Headers': "*"
                }
            })
            .then(resp => { 
                console.log(resp) 
                res()
            })
            .catch(e => {console.log("ERROR LOADING"); rej(e)})
        })
    }

    getAlarms() {
        console.log("GET - /alarms")

        return new Promise((res, rej) => {
            fetch(`${this._url}/alarms`, {method: 'GET'})
            .then(resp => {
                console.log(resp)
                return resp.json()})
            .then(data => {
                console.log(data)
                res(data)
            })
            .catch(e => {console.log("ERROR LOADING"); rej(e)})
        })
    }

    postAlarm(alarm) {
        console.log("POST - /alarms", JSON.stringify(alarm))
        return new Promise((res, rej) => {
            fetch(`${this._url}/alarms`, {
                method: 'POST',
                body: JSON.stringify(alarm)
            })
            .then(resp => 
            {
                console.log(resp)
                return resp.json()
                })
            .then(data => {
                console.log(data)
                res(data["id"])}) //return id as response
            .catch(e => {console.log("ERROR LOADING"); rej(e)})
        })
    }

    patchAlarm(newAlarm) { //newAlarm with existing ID
        console.log("PATCH", JSON.stringify(newAlarm))

        return new Promise((res, rej) => {
            fetch(`${this._url}/alarms/${newAlarm.id}`, {
                method: 'PATCH',
                body: JSON.stringify(newAlarm)
            })
            .then(resp => {
                console.log(resp)
                res()
            })
            .catch(e => {console.log("ERROR LOADING"); rej(e)})
        })
    }
}
