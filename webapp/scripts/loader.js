class Loader{
    constructor() {
        const elem = document.createElement('div')
        elem.classList.add('loader', 'blur-overlay', 'hidden')

        const inner = document.createElement('div')
        inner.classList.add('loader-inner')

        const spinner = document.createElement('div')
        spinner.classList.add('spinner')

        const txt = document.createElement('h1')
        
        this._defaultTxt = "Loading..."
        txt.textContent = this._defaultTxt

        inner.append(spinner, txt)
        elem.appendChild(inner)
        document.body.appendChild(elem)

        this._elem = elem
        this._txt = txt
    }

    show(txt) {
        this._txt.textContent = txt ?? this._defaultTxt
        this._elem.classList.remove('hidden')
    }

    hide() {
        this._elem.classList.add('hidden')
    }
}

class ErrorMsg extends Context{
    constructor() {
        const elem = document.createElement('div')
        const inner = document.createElement('div')
        inner.classList.add('error-inner')

        const innerWrap = document.createElement('div')
        
        const close = document.createElement('button')
        close.textContent = "x"
        close.classList.add('context-close-btn')

        const reconnect = document.createElement('button')
        reconnect.textContent = "Reconnect"
        reconnect.classList.add('reconn-btn')

        const changeIp = document.createElement('button')
        changeIp.textContent = "Change Ip"
        changeIp.classList.add('ip-change-btn')

        elem.classList.add('error', 'hidden', 'blur-overlay')

        const txt = document.createElement('p')
        txt.textContent = "Request failed - try again"

        innerWrap.append(txt, reconnect, changeIp, close)
        inner.appendChild(innerWrap)
        elem.appendChild(inner)
        
        
        super(elem)
        document.body.prepend(elem)

        this._reconn = reconnect
        this._changeIp = changeIp
    }

    addReconnHandler(handler) {
        this._reconn.addEventListener('click', handler)
    }

    addChangeIpHandler(handler) {
        this._changeIp.addEventListener('click', handler)
    }
}